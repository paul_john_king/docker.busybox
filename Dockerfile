# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG BUSYBOX_VERSION="1.31.0"

ARG WORK_DIR="/work"
ARG DOWNLOAD_DIR="${WORK_DIR}/downloads"
ARG SOURCE_DIR="${WORK_DIR}/sources"
ARG OBJECT_DIR="${WORK_DIR}/objects"
ARG TARGET_DIR="${WORK_DIR}/targets"

FROM "${WORKBENCH_IMAGE}" AS compile

	ARG BUSYBOX_VERSION

	ARG WORK_DIR
	ARG DOWNLOAD_DIR
	ARG SOURCE_DIR
	ARG OBJECT_DIR
	ARG TARGET_DIR

	ARG BUSYBOX_NAME="busybox-${BUSYBOX_VERSION}"
	ARG BUSYBOX_DOWNLOADS="https://www.busybox.net/downloads"

	ARG BUSYBOX_ARCHIVE_SUFFIX="tar.bz2"
	ARG BUSYBOX_ARCHIVE_NAME="${BUSYBOX_NAME}.${BUSYBOX_ARCHIVE_SUFFIX}"
	ARG BUSYBOX_ARCHIVE_URL="${BUSYBOX_DOWNLOADS}/${BUSYBOX_ARCHIVE_NAME}"
	ARG BUSYBOX_ARCHIVE_PATH="${DOWNLOAD_DIR}/${BUSYBOX_ARCHIVE_NAME}"

	ARG BUSYBOX_CHECKSUM_SUFFIX="sha256"
	ARG BUSYBOX_CHECKSUM_NAME="${BUSYBOX_ARCHIVE_NAME}.${BUSYBOX_CHECKSUM_SUFFIX}"
	ARG BUSYBOX_CHECKSUM_URL="${BUSYBOX_DOWNLOADS}/${BUSYBOX_CHECKSUM_NAME}"
	ARG BUSYBOX_CHECKSUM_PATH="${DOWNLOAD_DIR}/${BUSYBOX_CHECKSUM_NAME}"

	RUN \
		set -e; \
		set -u; \
		apt-get install --yes --quiet \
			libselinux1-dev=2.8-1build2; \
		mkdir -p "${DOWNLOAD_DIR}" "${SOURCE_DIR}" "${OBJECT_DIR}" "${TARGET_DIR}"; \
		cd "${DOWNLOAD_DIR}"; \
		curl --remote-name "${BUSYBOX_ARCHIVE_URL}"; \
		curl --remote-name "${BUSYBOX_CHECKSUM_URL}"; \
		sha256sum -c "${BUSYBOX_CHECKSUM_PATH}"; \
		cd "${SOURCE_DIR}"; \
		tar -xf "${BUSYBOX_ARCHIVE_PATH}"; \
		cd "${BUSYBOX_NAME}"; \
		make O="${OBJECT_DIR}" "allyesconfig"; \
		cd "${OBJECT_DIR}"; \
		sed \
			-e "s/.*CONFIG_DEBUG_SANITIZE=.*/# CONFIG_DEBUG_SANITIZE is not set/" \
			-e "s/.*CONFIG_PAM=.*/# CONFIG_PAM is not set/"\
			-e "s/.*CONFIG_WERROR=.*/# CONFIG_WERROR is not set/" \
			-i ".config"; \
		make; \
		cp "busybox" "${TARGET_DIR}"; \
		return;

	WORKDIR "${WORK_DIR}"

FROM "scratch" AS extract

	ARG WORKBENCH_IMAGE
	ARG BUSYBOX_VERSION

	ARG TARGET_DIR

	LABEL workbench.image="${WORKBENCH_IMAGE}"
	LABEL busybox.version="${BUSYBOX_VERSION}"

	COPY --from="compile" "${TARGET_DIR}/busybox" "/busybox"

	WORKDIR "/"

	ENTRYPOINT ["/busybox"]
